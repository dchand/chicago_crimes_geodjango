# python general imports
import os

# geodjango specific imports

from django.contrib.gis.db import models
from django.contrib.gis import forms

from django.contrib.gis.utils import LayerMapping



class Ward(models.Model):
    ward = models.CharField(max_length=4, primary_key=True)
    shape_leng = models.FloatField(null=True)
    shape_area = models.FloatField(null=True)
    geom = models.MultiPolygonField(srid=4269, null=True)
    objects = models.GeoManager()

    # Returns the string representation of the model.
    # Return string representation of Ward

    def __unicode__(self):
        return u'Ward ID is: %s' % (self.ward)

class Crime(models.Model):
    original_id = models.CharField(max_length=200)
    arrest = models.BooleanField(default=False)
    date = models.DateTimeField(db_index=True)
    domestic = models.BooleanField(default=False)
    latitude = models.DecimalField(max_digits=14, decimal_places=12)
    longitude = models.DecimalField(max_digits=14, decimal_places=12)
    primary_type = models.CharField(max_length=500)
    ward = models.ForeignKey(
            Ward,
            blank = True,
            null = True
    )

    # GeoDjango specific model attributes

    point = models.PointField(srid=4326, null=True, blank=True)
    objects = models.GeoManager()

    # Return string representation of Crime

    # def __unicode__(self):
    #     return '%s' % (self)

# Auto-generated `LayerMapping` dictionary for Ward model

ward_mapping = {
    'ward': 'WARD',
    'shape_leng': 'SHAPE_Leng',
    'shape_area': 'SHAPE_Area',
    'geom': 'MULTIPOLYGON',
}
