# Imports

# Django Management Command Imports

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

# GeoDjango Specific Imports

from django.contrib.gis.utils import LayerMapping

# Generic Python Library Imports

import os
import sys
import psycopg2

# Application Models Imports

from viewer.models import Crime, Ward, ward_mapping


class Command(BaseCommand):
    args = 'geography_type geography_type ...'
    help = 'Loads initial data for the model specified in the argument.'

    # Global variables for logic

    acceptable_choices = ['ward', 'crime']
    success = False
    case = None

    def handle(self, *args, **options):

        for model in args:
            try:
                self.case = self.acceptable_choices.index(model.lower())
            except Exception as e:
                error_text = "You specified an invalid geography. Please choose from: "
                for choice in self.acceptable_choices:
                    error_text += "%s," % (choice)
                raise CommandError(error_text)

        # Basic Data Loading functions

        if self.case == 0:  # Ward
            print "Beginning the Ward load."
            load_wards()
            self.success = True
            self.updateRegionGeom = True

        elif self.case == 1:  # Crime
            print "Beginning the Crime load."
            load_crimes()
            create_points_for_crimes()
            self.success = True

        else:
            print "You're fucked."

        # Output messages

        if self.success == True:
            self.stdout.write('Successfully loaded the data for: "%s"' % model)
        else:
            self.stdout.write('There was an error')


# Useful functions called in loaders.

def log_this(text):
    fullFile = __file__
    fullPath = os.getcwd()
    fileWithoutPath = fullFile.replace(fullPath, "").replace("/", "")
    fileName = fileWithoutPath + "__log.txt"

    logfile = open(fileName, 'a')
    logfile.write("\n %s" % (text))
    logfile.close


# Loading functions

def load_wards(verbose=True):
    # counties happen in two parts: 1) the shapefile and then 2) the csv of county,region,voters

    # Clear all objects

    print "Beginning phase 1: the shapefile loading"

    try:

        Ward.objects.all().delete()

    except Exception as e:

        print "There was an error deleting existing initial cases. The error is: %s" % (e)

    # Source data, mapping, execution
    wards_shp = '/opt/venv/chicago_crimes/viewer/data/shapefiles/WARDS_2015.shp'

    lm = LayerMapping(Ward, wards_shp, ward_mapping, transform=True, encoding='iso-8859-1')
    lm.save(strict=True, verbose=verbose)


def load_crimes(verbose=True):

    """
    This function clears the database of existing Crime objects and uses PSQL copy_from to load from CSV's.
    """

    try:

        Crime.objects.all().delete()
        print "Successfully deleted all existing objects."

    except Exception as e:

        print "There was an error deleting existing initial cases. The error is: %s" % (e)
        sys.exit()

    renamed_crimes = '/opt/venv/chicago_crimes/viewer/data/renamed_crimes/'
    os.chdir(renamed_crimes)

    for source_data_file in os.listdir(renamed_crimes):

        full_path_to_file = renamed_crimes + source_data_file
        conn = psycopg2.connect(
                "dbname='chicago_crimes' user='chicago_crimes' password='d5cda65eaa622604f9af738dad50339f' host='localhost'")

        cur = conn.cursor()
        table = 'viewer_crime'
        the_file = open(full_path_to_file)


        try:
            cur.copy_from(the_file, table, ',', columns=('arrest','date','domestic','original_id','latitude','longitude','primary_type','ward_id'))
            conn.commit()
            print "Executed COPY FROM on %s " % full_path_to_file

        except Exception as x:
            print "There was a problem with COPY FROM on %s " % full_path_to_file
            print x
            sys.exit()

        the_file.close()
        conn.close()

def create_points_for_crimes():


        print "About to query the database to create Point GEOM from (longitude, latitude) for each object."
        conn = psycopg2.connect(
                "dbname='chicago_crimes' user='chicago_crimes' password='d5cda65eaa622604f9af738dad50339f' host='localhost'")

        cur = conn.cursor()
        query_text = """UPDATE viewer_crime SET point = ST_SetSRID(ST_MakePoint(longitude,latitude),4326);"""

        try:
            cur.execute(query_text)
            conn.commit()
            conn.close()
            print "Successfully created Point GEOM for the objects."

        except Exception as e:
            print "There was a problem with UPDATE TABLE for the objects: %s" % (e)
            conn.close()
            sys.exit()







