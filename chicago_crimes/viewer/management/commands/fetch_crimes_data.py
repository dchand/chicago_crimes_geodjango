# Imports

# Django Management Command Imports

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option

# Generic Python Library Imports

import requests
import sys
import csv
import os


# file handling

class Command(BaseCommand):
    args = ''
    help = 'Fetches the .CSV formatted data from Chicago Data Portal'

    def handle(self, *args, **options):
        try:
            fetch_data()
            write_data_to_csv()

        except Exception as ex:
            print "There was an error at the command level: %s" % (ex)
            sys.exit()

        self.success = True

        # Output mesages

        if self.success == True:
            self.stdout.write('Successfully fetched the data for Chicago crimes.')


def get_total_amount_of_data():
    """This function uses the Chicago API to get the total number of records necessary for the fetch data function."""

    chicago_crimes_base_url = "https://data.cityofchicago.org/resource/6zsd-86xi.csv?"
    function_query = "$select=max(id)"
    url = chicago_crimes_base_url + function_query
    r = requests.get(url)
    amount = ''.join([c for c in r.text if c in '1234567890.'])
    return int(amount)


def write_data_to_csv():
    """This function takes fetched data from the API and writes them to new files that will have column names corresponding to the model fields. """

    raw_files_directory = '/opt/venv/chicago_crimes/viewer/data/raw_crimes/'
    processed_crimes_directory = '/opt/venv/chicago_crimes/viewer/data/renamed_crimes/'
    field_names = ["arrest", "date", "domestic", "original_id", "latitude", "longitude", "primary_type", "ward_id"]

    os.chdir(raw_files_directory)

    for fetched_file in os.listdir(raw_files_directory):

        csv_data_file = processed_crimes_directory + fetched_file

        with open(csv_data_file, 'w') as targetfile:

            writer = csv.DictWriter(targetfile, fieldnames=field_names)
            # writer.writeheader()

            reader = csv.DictReader(open(fetched_file, 'r'))
            print "Currently working on : %s" % (fetched_file)

            for row in reader:

                if any(field is None or field == '' for field in [row['ward'], row['latitude'], row['longitude']]):
                    continue

                writer.writerow({'original_id': row['id'],
                                 'date': row['date'],
                                 'primary_type': row['primary_type'],
                                 'arrest': row['arrest'].upper(),
                                 'domestic': row['domestic'],
                                 'ward_id': row['ward'],
                                 'latitude': row['latitude'],
                                 'longitude': row['longitude']
                                 })
            print "Successfully wrote the data from %s to %s" % (fetched_file, csv_data_file)


def fetch_data():
    """This function checks if the CSV file already exists. If that file exists,
    the function tells you that it exists, otherwise, it will fetch those data."""

    # variables for logic (magic strings/numbers)
    chicago_crimes_data_api_url = 'https://data.cityofchicago.org/resource/6zsd-86xi.csv?$select=id,date,primary_type,arrest,domestic,ward,latitude,longitude$offset=0$order=:id'

    chicago_crimes_base_url = "https://data.cityofchicago.org/resource/6zsd-86xi.csv?"
    fields_to_select = "$select=id,date,primary_type,arrest,domestic,ward,latitude,longitude"
    offset_string = "&$offset=SOMETHING"
    limit_string = "&$limit=SOMETHING"
    order_string = "&$order=:id"
    target_file = '/opt/venv/chicago_crimes/viewer/data/raw_crimes/SOMETHING.csv'
    header_authorisation = True

    offset_number = 0
    limit = 50000

    upper_limit = get_total_amount_of_data()

    while (offset_number + limit) < upper_limit:

        remaining = int(upper_limit) - int(offset_number)
        print "The offset number is %d, which means that there are %d records remaining." % (offset_number, remaining)

        offset_string = offset_string.replace('SOMETHING', str(offset_number))
        limit_string = limit_string.replace('SOMETHING', str(limit))
        target_csv_file = target_file.replace('SOMETHING', str(offset_number))
        target = open(target_csv_file, 'wb')

        url = chicago_crimes_base_url + fields_to_select + offset_string + limit_string + order_string
        r = requests.get(url)
        data = str(r.text)
        try:
            target.write(data)
            print "Successfully wrote to %s" % (target_csv_file)
        except Exception:
            print "There was an error. It was: %s" % (e)

        target.close()
        offset_number += limit
