# Imports

# Django Management Command Imports

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option

# Generic Python Library Imports

import subprocess
import sys
import os
import json
import glob

# Django Model imports

from viewer.models import Crime, Ward
from django.db.models import Count

# GeoDjango imports

from django.contrib.gis.geos import GeometryCollection, GEOSGeometry

# Disable database query logging
from django.conf import settings
if settings.DEBUG:
    from django.db import connection
    connection.use_debug_cursor = False


class Command(BaseCommand):
    args = ''
    help = 'Turns the created data into cached GeoJSON for JavaScript'

    def handle(self, *args, **options):
        try:
            create_geojson()
            convert_geojson_to_topojson()

        except Exception as ex:
            print "There was an error at the command level: %s" % (ex)
            sys.exit()

        self.success = True

        # Output mesages

        if self.success == True:
            self.stdout.write('Successfully created all GeoJSON for Chicago crimes.')


def create_geojson():
    # file handling

    geojson_files_directory = '/vagrant/static/geojsons/'
    os.chdir(geojson_files_directory)

    # function level vars

    # empty collections to fill
    geoReturn = {}
    features = []

    years_of_data = [a.year for a in set(Crime.objects.datetimes('date', 'year', order='DESC'))]
    ward_ids = Ward.objects.values('ward').distinct()

    wards = [item['ward'] for item in ward_ids]

    for year in years_of_data:

        for ward in wards:

            # create Django objects

            theWard = Ward.objects.get(pk=int(ward))
            theCrimes = theWard.crime_set.filter(date__year=year)

            aggregates = Crime.objects.values('primary_type') \
                .annotate(total_crime_count=Count('pk')) \
                .values('primary_type', 'total_crime_count') \
                .order_by('primary_type') \
                .filter(date__year=year) \
                .filter(ward_id=theWard.ward)

            # build the feature

            feature = {}
            properties = {}

            feature['type'] = "Feature"
            feature['geometry'] = json.loads(theWard.geom.json)
            properties['ward'] = ward
            properties['year'] = year
            properties['total_crimes'] = len(theCrimes)

            # loop through the aggregated crimes list and add each one as a property

            for aggregate in aggregates:
                properties[aggregate['primary_type']] = aggregate['total_crime_count']

            feature['properties'] = properties

            features.append(feature)

            # fill collections and assign to JSON
            geoReturn['type'] = 'FeatureCollection'
            geoReturn['features'] = features

        # print the JSON

        try:
            target_file = str(year) + ".json"
            file = open(target_file, 'w')

            json.dump(geoReturn, file, separators=(',', ':'))

            file.close()

            print "Successfully created GeoJSON for %s" % (target_file)

        except Exception as e:
            print "There was an error with the GeoJSON for %s. It was %s" % (year, e)
            continue


def convert_geojson_to_topojson():

    topojson_files_directory = '/vagrant/static/topojsons/'
    geojson_files_directory = "/vagrant/static/geojsons/*.json"

    for gj in glob.glob(geojson_files_directory):

        source = gj
        target = topojson_files_directory + gj.split('/')[-1].split('.')[0] + '.topo.json'

        """
        this is the bash command I would like to replicate in here with source and target
        using possibly subprocess and call

        (venv)root@vagrant-gisvm:/vagrant/static/node_modules/topojson# ./bin/topojson -o -p /vagrant/static/geojsons/2016.topo.json /vagrant/static/geojsons/2016.json
        """

        try:
            subprocess.check_call(['/vagrant/static/node_modules/topojson/bin/topojson', '-p', '-o', target, source])
            print "Successfully created topoJSON for : %s" % (target)

        except Exception as e:
            print "Error creating topoJSON for %s, from %s. The error is: %s" % (target, source, e)
            continue


