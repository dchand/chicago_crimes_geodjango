# Imports

    #Django Management Command Imports

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option


    # Generic Python Library Imports

import requests
import sys
import csv
import os

# file handling

class Command(BaseCommand):
    args = ''
    help = 'Fetches the ward shapefile from Chicago Data Portal'

    def handle(self, *args, **options):
        try:
            fetch_data()

        except Exception as ex:
            print "There was an error at the command level: %s" % (ex)
            sys.exit()

        self.success = True

        # Output mesages

        if self.success == True:
            self.stdout.write('Successfully fetched the boundaries for Chicago Wards.')

def fetch_data():

    """This function checks if the CSV file already exists. If that file exists, the function tells you that it exists, otherwise, it will fetch those data."""
    # variables for logic (magic strings/numbers)

    chicago_crimes_data_api_url = 'https://data.cityofchicago.org/api/geospatial/sp34-6z76?method=export&format=Shapefile'

    ward_shapefile = '/opt/venv/chicago_crimes/viewer/data/shapefiles/wards.zip'


    r = requests.get(chicago_crimes_data_api_url)
    with open(ward_shapefile, 'wb') as target:

        target.write(r.content)

    target.close()

