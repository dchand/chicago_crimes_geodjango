from django.contrib.gis import admin
from models import Ward, Crime

# Register your models here.
admin.site.register(Ward, admin.GeoModelAdmin)
admin.site.register(Crime, admin.GeoModelAdmin)
