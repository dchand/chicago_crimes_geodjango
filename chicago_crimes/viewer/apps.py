#viewer/apps.py

from django.apps import AppConfig

class ViewerConfig(AppConfig):
    name = 'viewer'
    verbose_name = 'Crime Data Viewer'