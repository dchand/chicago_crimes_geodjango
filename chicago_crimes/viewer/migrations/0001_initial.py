# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Crime',
            fields=[
                ('id', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('arrest', models.BooleanField(default=False)),
                ('date', models.DateTimeField()),
                ('domestic', models.BooleanField(default=False)),
                ('latitude', models.DecimalField(max_digits=14, decimal_places=12)),
                ('longitude', models.DecimalField(max_digits=14, decimal_places=12)),
                ('primary_type', models.CharField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ward',
            fields=[
                ('ward', models.CharField(max_length=5, serialize=False, primary_key=True)),
                ('shape_length', models.DecimalField(max_digits=14, decimal_places=12)),
                ('shape_area', models.DecimalField(max_digits=14, decimal_places=12)),
                ('mpoly', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='crime',
            name='ward',
            field=models.ForeignKey(to='viewer.Ward'),
            preserve_default=True,
        ),
    ]
