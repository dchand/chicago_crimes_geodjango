# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ward',
            name='mpoly',
        ),
        migrations.RemoveField(
            model_name='ward',
            name='shape_length',
        ),
        migrations.AddField(
            model_name='crime',
            name='point',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ward',
            name='geom',
            field=django.contrib.gis.db.models.fields.MultiPolygonField(srid=4269, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ward',
            name='shape_leng',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ward',
            name='shape_area',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='ward',
            name='ward',
            field=models.CharField(max_length=4, serialize=False, primary_key=True),
            preserve_default=True,
        ),
    ]
