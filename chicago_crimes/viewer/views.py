# Django Views imports

from django.shortcuts import render, get_list_or_404
from django.http import JsonResponse

# Django Models imports

from django.db.models import Count

# App Models imports

from .models import Crime


def index(request):
    years_for_data = [a.year for a in set(Crime.objects.datetimes('date', 'year', order='DESC'))]
    context = {
        'years_for_data': years_for_data
    }
    return render(
        request,
        'viewer/index.html',
        context
    )


def by_year(request, year):
    """
    The intended result here is to have aggregate data for wards for the year given.
    Example: in 2002, ward 17 had this many crimes, this many arrests, this many types of crimes, this many crimes/type

    This is to be JSON for JavaScript.

    Right now though this is the closest I can get:
    q=Crime.objects.values('ward').annotate(cc=Count('pk')).values('ward', 'cc').order_by('ward')

    What I would like to see added to this is a count by primary_type by ward, so, after we get the total_count_by ward, another level down
    would be ptypeA: 1, ptypeB:1, etc.
    """

    # crimes = Crime.objects.values().filter(date__year=year)

    crimes = Crime.objects.values('ward') \
        .annotate(total_crime_count=Count('pk')) \
        .values('ward', 'total_crime_count') \
        .order_by('ward') \
        .filter(date__year=year)


    return JsonResponse(list(crimes), safe=False)
