import shutil

def concatenate_files(src_files, dest_file):
    # Copy the first file.
    shutil.copy(src_files[0], dest_file)

    # Skip the header row, and copy in the rest of the rest of the files.
    with open(dest_file, 'a') as dest:
        for src_file in src_files[1:]:
            with open(src_file) as src:
                first_line = True
                for line in f.readlines():
                    if first_line:
                        # Skip the header row.
                        first_line = False
                    else:
                        # Copy in the rest of the file.
                        dest_file.write(line + '\n')